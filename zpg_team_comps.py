import os, sys
import numpy as np
import pandas
import itertools

class zpg_team_comps(object):
    def __init__(self,player_hero_ranks_csv):
        raw = pandas.read_csv(player_hero_ranks_csv).set_index('Hero').set_index('Type',append=True).sort_index(1)
        self.rank_data = raw
        ignore_degen = raw.replace(np.nan, '-', regex=True).replace('[^0-9]', '', regex=True)
        hero_choices = []
        player_ranks = []
        for key in ignore_degen:
            hero_choices.append([i for i,x in enumerate(ignore_degen[key].values) if x != ''])
            player_ranks.append([int(x) for i,x in enumerate(ignore_degen[key].values) if x != ''])
        all_possible_comps = list(itertools.product(*hero_choices))
        comp_ranks = [(np.average(x),np.std(x)) for x in list(itertools.product(*player_ranks))]     
        self.all_possible_comps = zpg_team_comps._prune_comps_with_duplicates(all_possible_comps,comp_ranks)

    def _all_comps_given_rank(rotations,desired_avg_rank_to_beat=np.infty):
        comps_given_rank = []
        for team,rank in rotations:
            if rank[0] <= desired_avg_rank_to_beat:
                comps_given_rank.append((*team,*rank))
        return comps_given_rank
    
    def _all_comps_given_rank_range(rotations,desired_avg_rank=(0,np.infty)):
        comps_given_rank = []
        for team,rank in rotations:
            if desired_avg_rank[0] < rank[0] <= desired_avg_rank[1]:
                comps_given_rank.append((*team,*rank))
        return comps_given_rank

    def _build_comps_dataframe(self,desired_avg_rank=np.infty):
        if type(desired_avg_rank) == tuple or type(desired_avg_rank) == list:
            comps = zpg_team_comps._all_comps_given_rank_range(self.all_possible_comps,desired_avg_rank=desired_avg_rank)
        else:
            comps = zpg_team_comps._all_comps_given_rank(self.all_possible_comps,desired_avg_rank_to_beat=desired_avg_rank)
        rank_dataframe = self.rank_data
        result = dict.fromkeys(rank_dataframe.columns.values)
        transpose = np.asarray(comps).T
        for i_player,heroes in enumerate(transpose[0:-2]):
            result[rank_dataframe.columns.values[i_player]]=[rank_dataframe.index.values[int(hero)][0] for hero in heroes]
        strategies = []
        compositions = []
        for comp in comps:
            composition = ''
            for i_hero in comp[0:-2]:
                composition += rank_dataframe.index.values[int(i_hero)][1]
            strategies.append(''.join(sorted(composition)))
            compositions.append(composition)
        result['strategy'] = strategies
        result['composition'] = compositions
        result['average rank'] = np.round(transpose[-2],2)
        result['variance'] = np.round(transpose[-1],2)
        result = pandas.DataFrame(result)
        result = result.set_index('strategy').set_index('average rank',append=True).set_index('variance',append=True).sort_index(0)
        return result

    def _prune_comps_with_duplicates(comps,ranks):
        result = []
        for team,rank in zip(comps,ranks):
            max_hero_occurence = np.max(list({i:team.count(i) for i in team}.values()))
            if max_hero_occurence == 1:
                result.append((team,rank))
        return result

    def _possible_teams_given_condition(comps_dataframe,compositions=None,conditions={}):
        try:
            # first apply strategy
            if compositions is not None:
                comps_dataframe = comps_dataframe.loc[compositions]

            # next apply player conditions
            for player in conditions:
                i_player = list(comps_dataframe.columns.values).index(player)
                def condition(row):
                    return row['composition'][i_player]==conditions[player]
                mask = comps_dataframe.apply(condition,axis=1)
                comps_dataframe = comps_dataframe[mask]
            return comps_dataframe
        except:
            if comps_dataframe.empty():
                return comps_dataframe
            else:
                print("An unhandled error occurred, please send the command used to Sulli")

    def possible_teams_given_conditions(self,desired_avg_rank=np.infty,compositions=None,conditions={}):
        ranked_comps = self._build_comps_dataframe(desired_avg_rank=desired_avg_rank)
        return zpg_team_comps._possible_teams_given_condition(ranked_comps,compositions=compositions,conditions=conditions)